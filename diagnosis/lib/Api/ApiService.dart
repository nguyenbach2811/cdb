import 'dart:convert';
import 'package:diagnosis/data/Model/DiagnosisModel.dart';
import 'package:flutter/services.dart' as rootBundle;
import '../data/Model/SymptomModel.dart';

class ApiService{
  Future<List<SymptomModel>> readSymJsonData() async {
    final symJsonData = await rootBundle.rootBundle.loadString('Json/symptom.json');
    final listSym = json.decode(symJsonData) as List<dynamic>;
    return listSym.map((e) => SymptomModel.fromJson(e)).toList();
  }
  Future<List<DiagnosisModel>> readDiaJsonData() async {
    final diaJsonData = await rootBundle.rootBundle.loadString('Json/disease.json');
    final listDia = json.decode(diaJsonData) as List<dynamic>;
    return listDia.map((e) => DiagnosisModel.fromJson(e)).toList();
  }
  // Future<List<SymptomModel>> getSymById(int id) async {
  //   final symJsonData = await rootBundle.rootBundle.loadString('Json/symptom.json');
  //   final listSym = json.decode(symJsonData) as List<dynamic>;
  //   return listSym.map((e) => SymptomModel.fromJson(e)).toList().where((element) => element.id == id).toList();
  //
  // }
  // Future<List<DiagnosisModel>> getDiaById(int id) async {
  //   final diaJsonData = await rootBundle.rootBundle.loadString('Json/disease.json');
  //   final listDia = json.decode(diaJsonData) as List<dynamic>;
  //   return listDia.map((e) => DiagnosisModel.fromJson(e)).toList().where((element) => element.id == id).toList();
  // }
}
class DiagnosisModel {
   int? id;
   String? diagnosisName;
   List<int> listIdOfSym =[];
   String? description;
   String? shortDes;
  DiagnosisModel({this.id, this.diagnosisName,required this.listIdOfSym, this.description, this.shortDes});
  DiagnosisModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    diagnosisName= json['DiagnosisName'];
    (json['ListIdOfSym'] as List).forEach((element) {listIdOfSym.add(element);});
    description= json['Description'];
    shortDes=json['ShortDes'];
  }
}

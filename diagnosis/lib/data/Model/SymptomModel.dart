class SymptomModel {
   int? id;
   String? symtomName;
   List<int> listIdOfDia = [];
   SymptomModel({this.id,this.symtomName,required this.listIdOfDia});
   SymptomModel.fromJson(Map<String, dynamic> json){
     id = json['id'];
     symtomName= json['SymptomName'];
     (json['ListIdOfDia'] as List).forEach((element) {listIdOfDia.add(element);});

   }
}

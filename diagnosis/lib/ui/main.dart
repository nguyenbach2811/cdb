import 'package:diagnosis/ui/Screen/DiseaseOfSymptom.dart';
import 'package:diagnosis/ui/Screen/Home.dart';
import 'package:flutter/material.dart';
import 'Screen/Disease.dart';
import 'package:diagnosis/data/Model/SymptomModel.dart';
import 'package:diagnosis/data/Model/DiagnosisModel.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
    );
  }
}


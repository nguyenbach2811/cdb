import 'package:diagnosis/ui/Screen/Diagnosis.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

import '../../Api/ApiService.dart';
import '../../data/Model/DiagnosisModel.dart';
import '../../data/Model/SymptomModel.dart';
import 'DetailDisease.dart';
import 'DiseaseOfSymptom.dart';
class AddMoreSym extends StatefulWidget {
  var dataFroPre=[];
  AddMoreSym({Key? key,required this.dataFroPre}) : super(key: key);

  @override
  State<AddMoreSym> createState() => _AddMoreSymState();
}

class _AddMoreSymState extends State<AddMoreSym> {
  static List<SymptomModel> list=[] ;
  static List<String> searchResultList =[];
  static List<String> sugesstionList=[];
  static List<String> historyList=[];
  TextEditingController inputControler =  TextEditingController();
  void initState(){
    getData();
    super.initState();
  }
  void getData() async {
    list = await ApiService().readSymJsonData();
    for(var e in list) sugesstionList.add(e.symtomName.toString());
    searchResultList = List.from(sugesstionList);
  }
  void filterList(String value) {
    searchResultList = sugesstionList
        .where((element) => element.toLowerCase().contains(value.toLowerCase()))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chuẩn đoán bệnh"),
      ),
      body: Padding(
          padding: EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SearchBar("Nhập thêm triệu chứng của bạn"),
                for(var e in widget.dataFroPre) showData(e),
              ],
            ),
          )),
    );
  }
  Widget SearchBar(String title)=> TypeAheadField(
    textFieldConfiguration: TextFieldConfiguration(
      controller: inputControler,
      onSubmitted: (String value) {
        filterList(value);
      },
      autofocus: false,
      style: DefaultTextStyle.of(context)
          .style
          .copyWith(fontStyle: FontStyle.normal,fontSize: 18,color:Colors.grey),
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.search),
        hintText: title,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(16.0)),
        focusColor: Colors.grey,
      ),
    ),
    suggestionsCallback: (pattern) {
      if(pattern.isEmpty) return historyList;
      else {
        filterList(pattern);
        return searchResultList;
      }
    },
    itemBuilder: (context, suggestion ) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(suggestion.toString()),
      );
    },
    onSuggestionSelected: (suggestion) {
      if (historyList.contains(suggestion.toString())) {
        historyList.remove(suggestion.toString());
      }
      late DiagnosisModel diagnosisModel ;
      for(var e in list){
        if(e.symtomName.toString().compareTo(suggestion)== 0){
          for(var q in widget.dataFroPre){
            for(var z in q.listIdOfSym) if(z==e.id) diagnosisModel=q;
          }
        }
      };
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailDiagnosis(diaModel: diagnosisModel),));
      print(diagnosisModel.diagnosisName.toString());
      inputControler.clear();
    },
  );
  Widget showData(DiagnosisModel data) => Padding(
    padding: const EdgeInsets.all(8.0),
    child: Card(
        elevation: 3,
        margin: EdgeInsets.symmetric(vertical: 10,horizontal: 20),
        child: ListTile(
          onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailDiagnosis(diaModel: data),)),
          title: Text(data.diagnosisName.toString(),style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold,color: Colors.black),),
          subtitle: Text(data.description.toString(),style: TextStyle(fontSize: 15,fontWeight: FontWeight.normal,color:Colors.grey),),
        )),
  );
}

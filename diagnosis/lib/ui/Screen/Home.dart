import 'package:diagnosis/ui/Screen/Diagnosis.dart';
import 'package:diagnosis/ui/Screen/Disease.dart';
import 'package:flutter/material.dart';
class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("Hệ thống chuẩn đoán bệnh")),
      ),
      body: Container(
        color: Colors.grey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Container(
                height: 200,
                color: Colors.white,
                child: Center(
                  child: TextButton(
                    child: Text("Chuẩn đoán bệnh",style: TextStyle(color: Colors.black),),
                    onPressed:() => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Diagnosis(),)),),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Container(
                height: 200,
                color: Colors.white,
                child: Center(
                  child: TextButton(
                      child: Text("Xem tất cả bệnh liên quan",style: TextStyle(color: Colors.black),),
                  onPressed:() => Navigator.of(context).push(MaterialPageRoute(builder: (context) => DiagnosisHome(),)),),
                ),
              ),
            ),
          ],
        ),

      ),
    );
  }

}

import 'package:diagnosis/data/Model/SymptomModel.dart';
import 'package:diagnosis/ui/Screen/AddMoreSym.dart';
import 'package:flutter/material.dart';
import '../../Api/ApiService.dart';
import '../../data/Model/DiagnosisModel.dart';
import 'DetailDisease.dart';

class ListDisOfSym extends StatefulWidget {
  final SymptomModel selectedSym;
  const ListDisOfSym({Key? key, required this.selectedSym}) : super(key: key);

  @override
  State<ListDisOfSym> createState() => _ListDisOfSymState();
}

class _ListDisOfSymState extends State<ListDisOfSym> {
  var item = [];
  // SymptomModel test = SymptomModel(listIdOfDia: [1],id: 1,symtomName: "Ho, Khàn tiếng");
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.selectedSym.symtomName.toString()),
      ),
      body: Container(
        color: Colors.blueGrey,
        child: FutureBuilder(
          future: ApiService().readDiaJsonData(),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return Text("${snapshot.error}");
            } else if (snapshot.hasData) {
              var listDia = snapshot.data as List<DiagnosisModel>;
              for (var e in listDia) {
                for (var q in widget.selectedSym.listIdOfDia) {
                  if (q == e.id) item.add(e);
                }
              }
              ;
              return ListView.builder(
                itemCount: item.length,
                itemBuilder: (context, index) {
                  return Card(
                      elevation: 3,
                      margin:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                      child: ListTile(
                        onTap: () =>
                            Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) =>
                              DetailDiagnosis(diaModel: item[index]),
                        )),
                        title: Text(
                          item[index].diagnosisName.toString(),
                          style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        ),
                        subtitle: Text(
                          item[index].description.toString(),
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              color: Colors.grey),
                        ),
                      ));
                },
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => showDialog(
          context: context,
          builder: (context) {
            {
              return AlertDialog(
                title: const Text('Bạn muốn tìm thêm triệu chứng không?'),
                content: const Text(
                    'Chọn Yes nếu muốn nhập thêm triệu chứng, không để quay về'),
                actions: <Widget>[
                  TextButton(
                    style: TextButton.styleFrom(
                      textStyle: Theme.of(context).textTheme.labelLarge,
                    ),
                    child: const Text('No'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  TextButton(
                    style: TextButton.styleFrom(
                      textStyle: Theme.of(context).textTheme.labelLarge,
                    ),
                    child: const Text('Yes'),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => AddMoreSym(dataFroPre: item),
                      ));
                    },
                  ),
                ],
              );
            }
          },
        ),
        backgroundColor: Colors.blue,
        child: const Icon(Icons.add),
      ),
    );
  }
}

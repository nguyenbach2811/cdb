import 'package:diagnosis/Api/ApiService.dart';
import 'package:flutter/material.dart';
import '../../data/Model/DiagnosisModel.dart';
import '../../data/Model/SymptomModel.dart';
class DetailDiagnosis extends StatefulWidget {
  final DiagnosisModel diaModel ;
  DetailDiagnosis({Key? key, required this.diaModel}) : super(key: key);
  @override
  State<DetailDiagnosis> createState() => _DetailDiagnosisState();
}

class _DetailDiagnosisState extends State<DetailDiagnosis> {
  static List<SymptomModel> list = [];
  void initState()  {
    super.initState();
    getSynData()async{
      list = await ApiService().readSymJsonData();
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.diaModel.diagnosisName.toString()),
      ),
      body: Column(
        children: [
          showInfor("Mô tả về bệnh", widget.diaModel.description.toString()),
        ],
      ),
    );
  }
  Widget showInfor(String title, String Content) => ListTile(
    title: Text(title,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
    subtitle:Text(Content,style: TextStyle(color: Colors.grey,fontWeight: FontWeight.normal,fontSize: 20)) ,
  );
   String getSymNameById(int id){
     String result ="";
    for(var e in list ){
      if(e.id==id) {
        result=e.symtomName.toString();
        break;}
    }
    return result;
  }

}


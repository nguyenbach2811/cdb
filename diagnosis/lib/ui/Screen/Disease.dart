import 'package:diagnosis/Api/ApiService.dart';
import 'package:diagnosis/data/Model/DiagnosisModel.dart';
import 'package:diagnosis/ui/Screen/DetailDisease.dart';
import 'package:flutter/material.dart';


class DiagnosisHome extends StatefulWidget {
  const DiagnosisHome({Key? key}) : super(key: key);

  @override
  State<DiagnosisHome> createState() => _DiagnosisHomeState();
}

class _DiagnosisHomeState extends State<DiagnosisHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text("Hệ thống chuẩn đoán bệnh ngoại khoa"),
      ),
      body:Container(
        color: Colors.blueGrey,
        child: FutureBuilder(
          future: ApiService().readDiaJsonData(),
          builder: (context, snapshot)  {
            if(snapshot.hasError){
              return Text("${snapshot.error}");
            }
            else if(snapshot.hasData)  {
              var item= snapshot.data as List<DiagnosisModel>;
              return ListView.builder(
                itemCount: item.length,
                itemBuilder: (context, index) {
                  return Card(
                    elevation: 3,
                    margin: EdgeInsets.symmetric(vertical: 10,horizontal: 20),
                    child: ListTile(
                      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailDiagnosis(diaModel: item[index]),)),
                      title: Text(item[index].diagnosisName.toString(),style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold,color: Colors.black),),
                      subtitle: Text(item[index].description.toString(),style: TextStyle(fontSize: 15,fontWeight: FontWeight.normal,color:Colors.grey),),
                  ));
                },
              );
            }
            else {
              return Center(child: CircularProgressIndicator(),);
            }
          },
        ),
      )
    );
  }
}

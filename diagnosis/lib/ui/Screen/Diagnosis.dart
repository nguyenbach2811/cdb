import 'package:diagnosis/Api/ApiService.dart';
import 'package:diagnosis/ui/Screen/DiseaseOfSymptom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import '../../data/Model/SymptomModel.dart';
class Diagnosis extends StatefulWidget {
  const Diagnosis({Key? key}) : super(key: key);

  @override
  State<Diagnosis> createState() => _DiagnosisState();
}

class _DiagnosisState extends State<Diagnosis> {
  static List<SymptomModel> list=[] ;
  static List<String> searchResultList =[];
  static List<String> sugesstionList=[];
  static List<String> historyList=[];
  TextEditingController inputControler =  TextEditingController();
  void initState(){
    getData();
    super.initState();
  }
  void getData() async {
    list = await ApiService().readSymJsonData();
    for(var e in list) sugesstionList.add(e.symtomName.toString());
    searchResultList = List.from(sugesstionList);
  }
  void filterList(String value) {
    searchResultList = sugesstionList
        .where((element) => element.toLowerCase().contains(value.toLowerCase()))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chuẩn đoán bệnh"),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
          child: SearchBar("Nhập triệu chứng của bạn")),
      );
  }
  Widget SearchBar(String title)=> TypeAheadField(
    textFieldConfiguration: TextFieldConfiguration(
      controller: inputControler,
      onSubmitted: (String value) {
        filterList(value);
      },
      autofocus: false,
      style: DefaultTextStyle.of(context)
          .style
          .copyWith(fontStyle: FontStyle.normal,fontSize: 18,color:Colors.grey),
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.search),
        hintText: title,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(16.0)),
        focusColor: Colors.grey,
      ),
    ),
    suggestionsCallback: (pattern) {
      if(pattern.isEmpty) return historyList;
      else {
        filterList(pattern);
        return searchResultList;
      }
    },
    itemBuilder: (context, suggestion ) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(suggestion.toString()),
      );
    },
    onSuggestionSelected: (suggestion) {
      if (historyList.contains(suggestion.toString())) {
        historyList.remove(suggestion.toString());
      }
      late SymptomModel suggestedSym ;
      for(var e in list){
        if(e.symtomName.toString().contains(suggestion)) suggestedSym=e;
      };
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => ListDisOfSym(selectedSym: suggestedSym),));
      inputControler.clear();
    },
  );

}
